import string
import time

def print_and_sleep(character, interval=1):
    print(character)
    time.sleep(interval)

def print_digits():
    for digit in string.digits:
        print_and_sleep(digit, 1)

def print_letters():
    for letter in string.ascii_letters:
        print_and_sleep(letter)

print_digits()
print_letters()
